package com.example.assessment;

import com.example.assessment.dto.seraphin.QuoteRequest;
import com.example.assessment.dto.seraphin.QuoteResponse;
import com.example.assessment.model.CoverAdvice;
import com.example.assessment.model.Customer;
import com.example.assessment.model.enumerations.CoverageCeilingFormula;
import com.example.assessment.model.enumerations.DeductibleFormula;
import com.example.assessment.model.enumerations.Premium;

import java.util.List;

public class Helpers {
    public final static String DEFAULT_CUSTOMER_FIRSTNAME = "Myfirstname";
    public final static String OTHER_CUSTOMER_FIRSTNAME = "MyOtherfirstname";
    public final static String DEFAULT_CUSTOMER_LASTNAME = "Mylastname";
    public final static String OTHER_CUSTOMER_LASTNAME = "MyOtherlastname";
    public final static String DEFAULT_CUSTOMER_EMAIL = "my.email@me.com";
    public final static String OTHER_CUSTOMER_EMAIL = "my.otheremail@me.com";
    public final static String DEFAULT_CUSTOMER_PHONENUMBER = "+32499123456";
    public final static String OTHER_CUSTOMER_PHONENUMBER = "+33491123456";
    public final static String DEFAULT_CUSTOMER_ADDRESS = "Rue Machin 1, 1000 Bruxelles";
    public final static String OTHER_CUSTOMER_ADDRESS = "Rue Machin 2, 1000 Bruxelles";


    public static Customer getDefaultCustomer(){
        return new Customer(
                DEFAULT_CUSTOMER_FIRSTNAME,
                DEFAULT_CUSTOMER_LASTNAME,
                DEFAULT_CUSTOMER_EMAIL,
                DEFAULT_CUSTOMER_PHONENUMBER,
                DEFAULT_CUSTOMER_ADDRESS);
    }

    public static Customer getOtherCustomer(){
        return new Customer(
                OTHER_CUSTOMER_FIRSTNAME,
                OTHER_CUSTOMER_LASTNAME,
                OTHER_CUSTOMER_EMAIL,
                OTHER_CUSTOMER_PHONENUMBER,
                OTHER_CUSTOMER_ADDRESS);
    }

    public static CoverAdvice getDefaultCoverAdvice() {
        return new CoverAdvice("86210", List.of(Premium.LEGAL_EXPENSES), CoverageCeilingFormula.large, DeductibleFormula.small);
    }

    public static QuoteRequest getDefaultQuoteRequest(Long customerId) {
        return new QuoteRequest(customerId, 80000L, "0649885171", "example SA", true, List.of("86210"), null, null);
    }

    public static QuoteResponse getDefaultQuoteResponse(){
        QuoteResponse.GrossPremiums premiums = new QuoteResponse.GrossPremiums();
        premiums.setLegalExpenses(123d);
        QuoteResponse.Data data = new QuoteResponse.Data();
        data.setAvailable(true);
        data.setQuoteId("123");
        data.setGrossPremiums(premiums);
        QuoteResponse quoteResponse = new QuoteResponse();
        quoteResponse.setData(data);
        return quoteResponse;
    }
}
