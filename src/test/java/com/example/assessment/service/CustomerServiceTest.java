package com.example.assessment.service;

import com.example.assessment.Helpers;
import com.example.assessment.model.Customer;
import com.example.assessment.repository.CustomerRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.persistence.EntityNotFoundException;
import java.util.List;

import static com.example.assessment.Helpers.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class CustomerServiceTest {

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private CustomerService customerService;

    @BeforeEach
    void setUp() {
        customerRepository.deleteAll();
    }

    @Test
    public void contextLoads() throws Exception {
        assertThat(customerService).isNotNull();
        assertThat(customerRepository).isNotNull();
    }

    @Test
    public void saveCustomer() {
        customerService.save(Helpers.getDefaultCustomer());

        List<Customer> saved = customerService.findAll();
        assertEquals(1, saved.size());

        Customer savedCustomer = saved.get(0);
        assertEquals(DEFAULT_CUSTOMER_FIRSTNAME, savedCustomer.getFirstName());
        assertEquals(DEFAULT_CUSTOMER_LASTNAME, savedCustomer.getLastName());
    }

    @Test
    public void findCustomerById(){
        Customer saved = customerService.save(Helpers.getDefaultCustomer());
        Customer customer = customerService.findOne(saved.getId());

        assertEquals(DEFAULT_CUSTOMER_FIRSTNAME, customer.getFirstName());
        assertEquals(DEFAULT_CUSTOMER_LASTNAME, customer.getLastName());
    }

    @Test
    public void findCustomersByLastName(){
        customerService.save(Helpers.getDefaultCustomer());
        customerService.save(Helpers.getOtherCustomer());

        List<Customer> customer = customerService.findByLastName(OTHER_CUSTOMER_LASTNAME);

        assertEquals(1, customer.size());
    }


    @Test
    public void findCustomerById_notFound(){
        Assertions.assertThrows(EntityNotFoundException.class, () -> customerService.findOne(1L));
    }
}