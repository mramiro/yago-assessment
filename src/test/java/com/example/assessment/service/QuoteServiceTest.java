package com.example.assessment.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class QuoteServiceTest {

    @Autowired
    private CoverAdvicesService coverAdvicesService;

    @Autowired
    private QuoteResponseWithAdvicesService quoteResponseWithAdvicesService;

    /**
     * Service tests omitted for this assessment
     */
    @Test
    public void contextLoads() throws Exception {
        assertThat(coverAdvicesService).isNotNull();
        assertThat(quoteResponseWithAdvicesService).isNotNull();
    }
}