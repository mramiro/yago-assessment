package com.example.assessment.service;

import com.example.assessment.repository.CoverAdviceRepository;
import com.example.assessment.repository.QuoteResponseWithAdvicesRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class CoverAdvicesServiceTest {

    @Autowired
    private CoverAdviceRepository repository;

    @BeforeEach
    void setUp() {
        repository.deleteAll();
    }

    /**
     * Service tests omitted for this assessment
     */
    @Test
    public void contextLoads() throws Exception {
        assertThat(repository).isNotNull();
    }
}