package com.example.assessment.config;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class SeraphinApiConfigurationTest {

    @Autowired
    private SeraphinApiConfiguration configuration;

    @Test
    void configurationLoads() {
        Assertions.assertNotNull(configuration);
        Assertions.assertEquals("https://staging-gtw.seraphin.be/fake-url-for-tests", configuration.getUrl());
        Assertions.assertEquals("fakeToken123", configuration.getApiToken());
    }

}