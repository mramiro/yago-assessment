package com.example.assessment.controller;

import com.example.assessment.Helpers;
import com.example.assessment.model.Customer;
import com.example.assessment.repository.CustomerRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static com.example.assessment.Helpers.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class CustomerControllerTest {

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private CustomerController customerController;

    @Autowired
    private MockMvc mockMvc;

    @BeforeEach
    void setUp() {
        customerRepository.deleteAll();
    }

    @Test
    public void contextLoads() throws Exception {
        assertThat(customerController).isNotNull();
        assertThat(customerRepository).isNotNull();
    }

    @Test
    public void saveCustomer() throws Exception {
        Customer newCustomer = Helpers.getDefaultCustomer();
        this.mockMvc
                .perform(post("/customer")
                        .content(new ObjectMapper().writeValueAsString(newCustomer))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName", is(DEFAULT_CUSTOMER_FIRSTNAME)))
                .andExpect(jsonPath("$.lastName", is(DEFAULT_CUSTOMER_LASTNAME)))
                .andExpect(jsonPath("$.email", is(DEFAULT_CUSTOMER_EMAIL)))
                .andExpect(jsonPath("$.phoneNumber", is(DEFAULT_CUSTOMER_PHONENUMBER)))
                .andExpect(jsonPath("$.address", is(DEFAULT_CUSTOMER_ADDRESS)));
    }

    @Test
    public void findCustomerById() throws Exception {
        Customer customer = customerRepository.save(Helpers.getDefaultCustomer());
        this.mockMvc
                .perform(get("/customer/" + customer.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName", is(DEFAULT_CUSTOMER_FIRSTNAME)))
                .andExpect(jsonPath("$.lastName", is(DEFAULT_CUSTOMER_LASTNAME)));
    }

    @Test
    public void findCustomerById_notFound() throws Exception {
        this.mockMvc
                .perform(get("/customer/1"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void findAllCustomers() throws Exception {
        customerRepository.save(Helpers.getDefaultCustomer());
        customerRepository.save(Helpers.getDefaultCustomer());

        this.mockMvc
                .perform(get("/customer")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)));
    }
}