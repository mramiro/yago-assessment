package com.example.assessment.controller;

import com.example.assessment.Helpers;
import com.example.assessment.dto.seraphin.QuoteRequest;
import com.example.assessment.dto.seraphin.QuoteResponse;
import com.example.assessment.external.InsuranceCompanyApi;
import com.example.assessment.model.CoverAdvice;
import com.example.assessment.model.Customer;
import com.example.assessment.model.QuoteResponseWithAdvices;
import com.example.assessment.model.enumerations.CoverageCeilingFormula;
import com.example.assessment.model.enumerations.DeductibleFormula;
import com.example.assessment.model.enumerations.Premium;
import com.example.assessment.repository.CoverAdviceRepository;
import com.example.assessment.repository.CustomerRepository;
import com.example.assessment.repository.QuoteResponseWithAdvicesRepository;
import com.example.assessment.service.QuoteService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.ArrayList;
import java.util.List;

import static com.example.assessment.Helpers.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.any;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class QuoteControllerTest {

    @Autowired
    private QuoteController controller;
    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private CoverAdviceRepository coverAdviceRepository;
    @Autowired
    private QuoteResponseWithAdvicesRepository quoteResponseWithAdvicesRepository;

    @InjectMocks
    private QuoteService quoteService;
    @MockBean
    private InsuranceCompanyApi insuranceCompanyApi;

    @Autowired
    private MockMvc mockMvc;

    @BeforeEach
    void setUp() {
        customerRepository.deleteAll();
        coverAdviceRepository.deleteAll();
        quoteResponseWithAdvicesRepository.deleteAll();

        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void contextLoads() throws Exception {
        assertThat(controller).isNotNull();
    }

    @Test
    public void getQuote() throws Exception {
        // setup customer and save it in database
        Customer customer = customerRepository.save(Helpers.getDefaultCustomer());

        // setup advice and save it in database
        CoverAdvice coverAdvice = coverAdviceRepository.save(Helpers.getDefaultCoverAdvice());

        // setup quote request before and after the advices have been added
        QuoteRequest quoteRequest = Helpers.getDefaultQuoteRequest(customer.getId());
        QuoteRequest quoteRequestToInsurer = Helpers.getDefaultQuoteRequest(customer.getId());
        quoteRequestToInsurer.setDeductibleFormula(DeductibleFormula.small);
        quoteRequestToInsurer.setCoverageCeilingFormula(CoverageCeilingFormula.large);

        // setup and mock call to insurer
        QuoteResponse quoteResponse = Helpers.getDefaultQuoteResponse();
        Mockito.when(insuranceCompanyApi.getQuote(quoteRequestToInsurer)).thenReturn(quoteResponse);

        this.mockMvc
                .perform(post("/quote-with-advices")
                        .content(new ObjectMapper().writeValueAsString(quoteRequest))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        Iterable<QuoteResponseWithAdvices> quotes = quoteResponseWithAdvicesRepository.findAll();
        List<QuoteResponseWithAdvices> result = new ArrayList<>();
        quotes.forEach(result::add);

        assertEquals(1, result.size());
        assertEquals(customer.getId(), result.get(0).getCustomerId());
        assertEquals(1, result.get(0).getCoverAdvices().size());
        assertEquals(coverAdvice.getId(), result.get(0).getCoverAdvices().get(0).getId());
        // the other properties are not tested for this assessment
    }
}