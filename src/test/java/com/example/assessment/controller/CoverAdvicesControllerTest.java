package com.example.assessment.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@AutoConfigureMockMvc
class CoverAdvicesControllerTest {

    @Autowired
    private CoverAdvicesController controller;

    /**
     * Controller tests omitted for this assessment
     */
    @Test
    public void contextLoads() throws Exception {
        assertThat(controller).isNotNull();
    }
}