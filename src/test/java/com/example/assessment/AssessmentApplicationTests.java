package com.example.assessment;

import com.example.assessment.controller.CustomerController;
import com.example.assessment.repository.CustomerRepository;
import com.example.assessment.service.CustomerService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class AssessmentApplicationTests {

	@Autowired
	private CustomerController customerController;

	@Autowired
	private CustomerService customerService;

	@Autowired
	private CustomerRepository customerRepository;

	@Test
	void contextLoads() {
		Assertions.assertNotNull(customerController);
		Assertions.assertNotNull(customerService);
		Assertions.assertNotNull(customerRepository);
	}
}
