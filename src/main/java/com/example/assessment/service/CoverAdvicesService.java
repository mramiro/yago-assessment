package com.example.assessment.service;

import com.example.assessment.model.CoverAdvice;
import com.example.assessment.repository.CoverAdviceRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class CoverAdvicesService {

    // TODO this should be stored as locales and translated for each supported language (see https://www.digitalocean.com/community/tutorials/java-i18n-internationalization-in-java)
    private static final String AFTER_DELIVERY_DESCRIPTION = "Covers damage arising after delivery of or completion of work (ex: new machines recently installed at the client's office start a fire).";
    private static final String PUBLIC_LIABILITY_DESCRIPTION = "Cover compensation claims for injury or damage (ex: you spill a cup of coffee over a client’s computer equipment).";
    private static final String PROFESSIONAL_INDEMNITY_DESCRIPTION = "Cover compensation claims for a mistake that you make during your work (ex: accidentally forwarded confidential client information to third parties).";
    private static final String ENTRUSTED_OBJECTS_DESCRIPTION = "Objects that don't belong to you, and are entrusted to you. You are obviously liable for any damage to these goods. (ex: you break the super expensive computer that was provided to you as an IT consultant).";
    private static final String LEGAL_EXPENSES_DESCRIPTION = "Also known as legal insurance, is an insurance which facilitates access to law and justice by providing legal advice and covering legal costs of a dispute. (ex: a client asks you for a financial compensation for a mistake you made in your work and you consider it's absolutely not you fault considering the context and you thus want to hire a lawyer to defend you).";

    private CoverAdviceRepository coverAdviceRepository;

    public CoverAdvicesService(CoverAdviceRepository coverAdviceRepository) {
        this.coverAdviceRepository = coverAdviceRepository;
    }

    public List<CoverAdvice> findAll() {
        Iterable<CoverAdvice> coverAdvices = coverAdviceRepository.findAll();
        List<CoverAdvice> result = new ArrayList<>();
        coverAdvices.forEach(result::add);

        log.debug("Found {} advices", result.size());

        return result;
    }

    public CoverAdvice findOne(String nacebelId) {
        log.debug("Looking for advices with nacebelId: {}", nacebelId);
        CoverAdvice coverAdvice = coverAdviceRepository
                .findByNacebelId(nacebelId)
                .orElseThrow(EntityNotFoundException::new);
        log.debug("Found advice {}", coverAdvice);

        return coverAdvice;
    }

    public List<CoverAdvice> findMultiple(List<String> nacebelIds) {
        log.debug("Looking for advices with nacebelIds: {}", nacebelIds);
        List<CoverAdvice> coverAdvices = coverAdviceRepository
                .findAllByNacebelId(nacebelIds)
                .orElseThrow(EntityNotFoundException::new);
        log.debug("Found {} advice(s)", coverAdvices);
        return coverAdvices;
    }

    public CoverAdvice save(CoverAdvice coverAdvice) {
        try {
            return coverAdviceRepository.save(coverAdvice);
        } catch (DataIntegrityViolationException e) {
            log.error("Could not persist entity {}", coverAdvice, e);
            return null;
        }
    }
}