package com.example.assessment.service;

import com.example.assessment.model.Customer;
import com.example.assessment.repository.CustomerRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class CustomerService {

    private final CustomerRepository customerRepository;

    public CustomerService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public List<Customer> findAll(){
        Iterable<Customer> customers = customerRepository.findAll();
        List<Customer> result = new ArrayList<>();
        customers.forEach(result::add);

        log.debug("Found {} customers", result.size());

        return result;
    }

    public Customer save(Customer customer){
        return customerRepository.save(customer);
    }

    public Customer findOne(Long id){
        log.debug("Looking for user with id: {}", id);
        Customer customer = customerRepository
                .findById(id)
                .orElseThrow(EntityNotFoundException::new);
        log.debug("Found customer {}", customer);

        return customer;
    }

    public List<Customer> findByLastName(String lastname){
        log.debug("Looking for customers with name: {}", lastname);
        List<Customer> customers = customerRepository
                .findByLastName(lastname)
                .orElseThrow(EntityNotFoundException::new);
        log.debug("Found customer(s) {}", customers);

        return customers;
    }
}
