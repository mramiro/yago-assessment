package com.example.assessment.service;

import com.example.assessment.model.QuoteResponseWithAdvices;
import com.example.assessment.dto.seraphin.QuoteRequest;
import com.example.assessment.dto.seraphin.QuoteResponse;
import com.example.assessment.external.InsuranceCompanyApi;
import com.example.assessment.model.CoverAdvice;
import com.example.assessment.model.enumerations.CoverageCeilingFormula;
import com.example.assessment.model.enumerations.DeductibleFormula;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class QuoteService {

    private List<InsuranceCompanyApi> providers = new ArrayList<>();
    private final CoverAdvicesService coverAdvicesService;
    private final QuoteResponseWithAdvicesService quoteResponseWithAdvicesService;

    public QuoteService(InsuranceCompanyApi provider, CoverAdvicesService coverAdvicesService, QuoteResponseWithAdvicesService quoteResponseWithAdvicesService) {
        this.coverAdvicesService = coverAdvicesService;
        this.quoteResponseWithAdvicesService = quoteResponseWithAdvicesService;
        // this should be populated with every insurance provider
        this.providers.add(provider);
    }

    public List<QuoteResponse> getQuote(QuoteRequest quoteRequest){
        List<QuoteResponse> quoteResponses= new ArrayList<>();
        log.debug("Getting quotes from providers");
        providers.forEach(provider -> {
            QuoteResponse quoteresponse = provider.getQuote(quoteRequest);
            if (quoteresponse != null) {
                log.debug("QuoteResponse: {}", quoteresponse);
                quoteResponses.add(quoteresponse);
            }
        });
        return quoteResponses;
    }

    public List<QuoteResponseWithAdvices> getQuoteWithAdvices(QuoteRequest quoteRequest) {
        List<String> nacebelIds = quoteRequest.getNacebelCodes();
        log.debug("Looking for quote with nacebelIds: {}", nacebelIds);
        List<CoverAdvice> coverAdvices = coverAdvicesService.findMultiple(nacebelIds);

        quoteRequest.setDeductibleFormula(findLargestDeductibleFormulaFrom(coverAdvices));
        quoteRequest.setCoverageCeilingFormula(findLargestCeilingFormulaFrom(coverAdvices));
        log.debug("Selected deductible formula: {}", quoteRequest.getDeductibleFormula());
        log.debug("Selected coverage ceiling formula: {}", quoteRequest.getCoverageCeilingFormula());

        List<QuoteResponse> quoteResponses = this.getQuote(quoteRequest);
        List<QuoteResponseWithAdvices> quoteResponseWithAdvicesList = new ArrayList<>();

        quoteResponses.forEach(quoteResponse -> {
            QuoteResponseWithAdvices quote = new QuoteResponseWithAdvices();
            quote.setCustomerId(quoteRequest.getCustomerId());
            quote.setCoverageCeiling(quoteResponse.getData().getCoverageCeiling());
            quote.setDeductible(quoteResponse.getData().getDeductible());
            quote.setQuoteId(quoteResponse.getData().getQuoteId());
            quote.setAfterDelivery(quoteResponse.getData().getGrossPremiums().getAfterDelivery());
            quote.setPublicLiability(quoteResponse.getData().getGrossPremiums().getPublicLiability());
            quote.setProfessionalIndemnity(quoteResponse.getData().getGrossPremiums().getProfessionalIndemnity());
            quote.setEntrustedObjects(quoteResponse.getData().getGrossPremiums().getEntrustedObjects());
            quote.setLegalExpenses(quoteResponse.getData().getGrossPremiums().getLegalExpenses());
            quote.setCoverAdvices(coverAdvices);

            quote = quoteResponseWithAdvicesService.save(quote);

            quoteResponseWithAdvicesList.add(quote);
        });

        return quoteResponseWithAdvicesList;
    }

    private DeductibleFormula findLargestDeductibleFormulaFrom(List<CoverAdvice> coverAdvices) {
        List<DeductibleFormula> formulas = coverAdvices.stream()
                .map(CoverAdvice::getDeductibleFormula)
                .collect(Collectors.toList());
        if (formulas.contains(DeductibleFormula.large)){
            return DeductibleFormula.large;
        }
        if (formulas.contains(DeductibleFormula.medium)){
            return DeductibleFormula.medium;
        }
        return DeductibleFormula.small;
    }

    private CoverageCeilingFormula findLargestCeilingFormulaFrom(List<CoverAdvice> coverAdvices) {
        List<CoverageCeilingFormula> formulas = coverAdvices.stream()
                .map(CoverAdvice::getCoverageCeilingFormula)
                .collect(Collectors.toList());
        if (formulas.contains(CoverageCeilingFormula.large)){
            return CoverageCeilingFormula.large;
        }
        return CoverageCeilingFormula.small;
    }
}
