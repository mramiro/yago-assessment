package com.example.assessment.service;

import com.example.assessment.model.Customer;
import com.example.assessment.model.QuoteResponseWithAdvices;
import com.example.assessment.repository.QuoteResponseWithAdvicesRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class QuoteResponseWithAdvicesService {

    private final QuoteResponseWithAdvicesRepository repository;

    public QuoteResponseWithAdvicesService(QuoteResponseWithAdvicesRepository repository) {
        this.repository = repository;
    }

    public QuoteResponseWithAdvices save(QuoteResponseWithAdvices quoteResponseWithAdvices){
        return repository.save(quoteResponseWithAdvices);
    }

    public QuoteResponseWithAdvices findOne(Long id){
        log.debug("Looking for quoteResponseWithAdvices with id: {}", id);
        QuoteResponseWithAdvices customer = repository
                .findById(id)
                .orElseThrow(EntityNotFoundException::new);
        log.debug("Found quoteResponseWithAdvices {}", customer);

        return customer;
    }

    public List<QuoteResponseWithAdvices> findAll(){
        Iterable<QuoteResponseWithAdvices> customers = repository.findAll();
        List<QuoteResponseWithAdvices> result = new ArrayList<>();
        customers.forEach(result::add);

        log.debug("Found {} customers", result.size());

        return result;
    }
}
