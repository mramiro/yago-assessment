package com.example.assessment.repository;

import com.example.assessment.model.CoverAdvice;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CoverAdviceRepository extends CrudRepository<CoverAdvice, Long> {

    Optional<CoverAdvice> findByNacebelId(String nacebelId);

    @Query(value = "select ca from CoverAdvice ca where ca.nacebelId in ?1")
    Optional<List<CoverAdvice>> findAllByNacebelId(List<String> nacebelIds);
}