package com.example.assessment.repository;

import com.example.assessment.model.QuoteResponseWithAdvices;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QuoteResponseWithAdvicesRepository extends CrudRepository<QuoteResponseWithAdvices, Long> {
}
