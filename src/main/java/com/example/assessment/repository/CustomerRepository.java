package com.example.assessment.repository;

import java.util.List;
import java.util.Optional;

import com.example.assessment.model.Customer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends CrudRepository<Customer, Long> {

    Optional<List<Customer>> findByLastName(String lastName);
}