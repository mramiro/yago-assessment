package com.example.assessment.config;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@ToString
@Configuration
@ConfigurationProperties(prefix = "insurance.seraphin")
public class SeraphinApiConfiguration {

    private String url;
    private String apiToken;
}