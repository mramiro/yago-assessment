package com.example.assessment.controller;

import com.example.assessment.model.CoverAdvice;
import com.example.assessment.service.CoverAdvicesService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@Controller
@Slf4j
public class CoverAdvicesController {

    private final CoverAdvicesService coverAdvicesService;

    public CoverAdvicesController(CoverAdvicesService coverAdvicesService) {
        this.coverAdvicesService = coverAdvicesService;
    }

    @GetMapping("/advice")
    public ResponseEntity<Iterable<CoverAdvice>> getAllAdvices(){
        log.info("Endpoint - get all advices");
        return ResponseEntity.ok(coverAdvicesService.findAll());
    }

    @GetMapping("/advice/search")
    public ResponseEntity<List<CoverAdvice>> getAdviceByNacebelId(@RequestParam List<String> nacebelIds){
        try {
            if (nacebelIds.size() > 0){
                log.info("Endpoint - get advice for nacebelIds {}", nacebelIds);
                return ResponseEntity.ok(coverAdvicesService.findMultiple(nacebelIds));
            }
        } catch (EntityNotFoundException e){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.badRequest().build();
    }

    @PostMapping("/advice")
    public ResponseEntity<CoverAdvice> saveAdvice(@RequestBody @Valid CoverAdvice coverAdvice) {
        log.info("Endpoint - create cover advice with data {}", coverAdvice.toString());
        return ResponseEntity.ok(coverAdvicesService.save(coverAdvice));
    }
}
