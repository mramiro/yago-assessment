package com.example.assessment.controller;

import com.example.assessment.model.Customer;
import com.example.assessment.service.CustomerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@Controller
@Slf4j
public class CustomerController {

    private final CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping("/customer")
    public ResponseEntity<Iterable<Customer>> getCustomers(){
        log.info("Endpoint - get all customers");
        return ResponseEntity.ok(customerService.findAll());
    }

    @GetMapping("/customer/{id}")
    public ResponseEntity<Customer> getCustomerById(@PathVariable Long id){
        try {
            log.info("Endpoint - get customer for id {}", id);
            return ResponseEntity.ok(customerService.findOne(id));
        } catch (EntityNotFoundException e){
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping("/customer")
    public ResponseEntity<Customer> saveCustomer(@RequestBody @Valid Customer customer) {
        log.info("Endpoint - create customer with data {}", customer.toString());
        return ResponseEntity.ok(customerService.save(customer));
    }
}
