package com.example.assessment.controller;

import com.example.assessment.model.QuoteResponseWithAdvices;
import com.example.assessment.dto.seraphin.QuoteRequest;
import com.example.assessment.service.QuoteResponseWithAdvicesService;
import com.example.assessment.service.QuoteService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@Controller
@Slf4j
public class QuoteController {

    private final QuoteService quoteService;
    private final QuoteResponseWithAdvicesService quoteResponseWithAdvicesService;

    public QuoteController(QuoteService quoteService, QuoteResponseWithAdvicesService quoteResponseWithAdvicesService) {
        this.quoteService = quoteService;
        this.quoteResponseWithAdvicesService = quoteResponseWithAdvicesService;
    }

    @PostMapping("/quote")
    public ResponseEntity<Void> getQuote(@RequestBody QuoteRequest quoteRequest){
        log.debug("Endpoint called: '/quote' with parameters {}", quoteRequest);
        quoteService.getQuote(quoteRequest);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/quote-with-advices")
    public ResponseEntity<List<QuoteResponseWithAdvices>> getQuoteWithAdvices(@RequestBody QuoteRequest quoteRequest){
        log.debug("Endpoint called: '/quote-with-advices' with parameters {}", quoteRequest);
        return ResponseEntity.ok(quoteService.getQuoteWithAdvices(quoteRequest));
    }

    @GetMapping("/quote/{quoteId}")
    public ResponseEntity<QuoteResponseWithAdvices> getQuoteWithAdvicesFromId(@PathVariable Long quoteId){
        log.debug("Endpoint called: '/quote/{}'", quoteId);
        try {
            return ResponseEntity.ok(quoteResponseWithAdvicesService.findOne(quoteId));
        } catch (EntityNotFoundException e){
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/quote")
    public ResponseEntity<List<QuoteResponseWithAdvices>> getAllQuotes(){
        log.debug("Endpoint called: '/quote");
        try {
            return ResponseEntity.ok(quoteResponseWithAdvicesService.findAll());
        } catch (EntityNotFoundException e){
            return ResponseEntity.notFound().build();
        }
    }
}
