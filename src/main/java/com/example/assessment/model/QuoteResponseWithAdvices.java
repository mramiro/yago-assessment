package com.example.assessment.model;

import com.example.assessment.dto.seraphin.QuoteResponse;
import com.example.assessment.model.enumerations.Premium;
import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@ToString
public class QuoteResponseWithAdvices {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    private Long customerId;
    private Integer coverageCeiling;
    private Integer deductible;
    private String quoteId;
    private Double afterDelivery;
    private Double publicLiability;
    private Double professionalIndemnity;
    private Double entrustedObjects;
    private Double legalExpenses;

    @ManyToMany(fetch = FetchType.EAGER)
    private List<CoverAdvice> coverAdvices;
}
