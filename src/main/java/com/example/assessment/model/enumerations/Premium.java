package com.example.assessment.model.enumerations;

public enum Premium {
    AFTER_DELIVERY, PUBLIC_LIABILITY, PROFESSIONAL_INDEMNITY, ENTRUSTED_OBJECTS, LEGAL_EXPENSES
}
