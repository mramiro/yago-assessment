package com.example.assessment.model.enumerations;

public enum DeductibleFormula {
    // TODO use a proper enums with uppercase elements and lowercase labels
    small, medium, large;
}
