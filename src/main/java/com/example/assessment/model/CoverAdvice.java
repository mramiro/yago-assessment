package com.example.assessment.model;

import com.example.assessment.model.enumerations.CoverageCeilingFormula;
import com.example.assessment.model.enumerations.DeductibleFormula;
import com.example.assessment.model.enumerations.Premium;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Data
public class CoverAdvice {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @Size(min = 5, max = 5)
    @Column(unique=true)
    private String nacebelId;

    @Enumerated(EnumType.STRING)
    @ElementCollection
    @CollectionTable(name = "premiums", joinColumns = @JoinColumn(name = "id"))
    @Column(name = "premiumsList")
    private List<Premium> premiums;

    @Enumerated(EnumType.STRING)
    private CoverageCeilingFormula coverageCeilingFormula;

    @Enumerated(EnumType.STRING)
    private DeductibleFormula deductibleFormula;

    protected CoverAdvice() {}

    public CoverAdvice(String nacebelId, List<Premium> premiums, CoverageCeilingFormula coverageCeilingFormula, DeductibleFormula deductibleFormula) {
        this.nacebelId = nacebelId;
        this.premiums = premiums;
        this.coverageCeilingFormula = coverageCeilingFormula;
        this.deductibleFormula = deductibleFormula;
    }
}