package com.example.assessment.dto.seraphin;

import com.example.assessment.model.enumerations.CoverageCeilingFormula;
import com.example.assessment.model.enumerations.DeductibleFormula;
import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString
public class QuoteRequest {

    // TODO add validation for each field

    private Long customerId;
    private Long annualRevenue;
    private String enterpriseNumber;
    private String legalName;
    private Boolean naturalPerson;
    private List<String> nacebelCodes;
    private DeductibleFormula deductibleFormula;
    private CoverageCeilingFormula coverageCeilingFormula;

    public QuoteRequest(Long customerId, Long annualRevenue, String enterpriseNumber, String legalName, Boolean naturalPerson, List<String> nacebelCodes, DeductibleFormula deductibleFormula, CoverageCeilingFormula coverageCeilingFormula) {
        this.customerId = customerId;
        this.annualRevenue = annualRevenue;
        this.enterpriseNumber = enterpriseNumber;
        this.legalName = legalName;
        this.naturalPerson = naturalPerson;
        this.nacebelCodes = nacebelCodes;
        this.deductibleFormula = deductibleFormula;
        this.coverageCeilingFormula = coverageCeilingFormula;
    }
}
