package com.example.assessment.dto.seraphin;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class QuoteResponse {

    private Boolean success;
    private String message;
    private Data data;

    @lombok.Data
    @ToString
    public static class Data {
        private Boolean available;
        private Integer coverageCeiling;
        private Integer deductible;
        private String quoteId;
        private GrossPremiums grossPremiums;
        private String message;
    }

    @lombok.Data
    @ToString
    public static class GrossPremiums {
        private Double afterDelivery;
        private Double publicLiability;
        private Double professionalIndemnity;
        private Double entrustedObjects;
        private Double legalExpenses;
    }
}