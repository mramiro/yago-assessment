package com.example.assessment.external;

import com.example.assessment.config.SeraphinApiConfiguration;
import com.example.assessment.dto.seraphin.QuoteRequest;
import com.example.assessment.dto.seraphin.QuoteResponse;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import lombok.extern.slf4j.Slf4j;
import okhttp3.*;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
@Slf4j
public class SeraphinApi implements InsuranceCompanyApi{

    public static final MediaType MEDIA_TYPE_JSON = MediaType.parse("application/json; charset=utf-8");

    private final SeraphinApiConfiguration seraphinApiConfiguration;

    public SeraphinApi(SeraphinApiConfiguration seraphinApiConfiguration) {
        this.seraphinApiConfiguration = seraphinApiConfiguration;
    }

    @Override
    public QuoteResponse getQuote(QuoteRequest quoteRequest) {
        OkHttpClient client = new OkHttpClient();

        Moshi moshi = new Moshi.Builder().build();
        JsonAdapter<QuoteRequest> quoteRequestAdapter = moshi.adapter(QuoteRequest.class);
        JsonAdapter<QuoteResponse> quoteResponseAdapter = moshi.adapter(QuoteResponse.class);

        String json = quoteRequestAdapter.toJson(quoteRequest);
        log.debug("JSON to send to Seraphin: {}", json);

        Request request = new Request.Builder()
                .url(seraphinApiConfiguration.getUrl())
                .header("x-api-key", seraphinApiConfiguration.getApiToken())
                .post(RequestBody.create(json, MEDIA_TYPE_JSON))
                .build();

        try (Response response = client.newCall(request).execute()) {
            if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);

            QuoteResponse quoteResponse = quoteResponseAdapter.fromJson(response.body().source());
            if (!quoteResponse.getSuccess()) throw new IOException("Unexpected response from Seraphin " + quoteResponse.getData().getMessage());

            return quoteResponse;
        } catch (IOException e) {
            log.error("Could not get Quote from Seraphin", e);
        }
        return null;
    }
}
