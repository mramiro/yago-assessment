package com.example.assessment.external;

import com.example.assessment.dto.seraphin.QuoteRequest;
import com.example.assessment.dto.seraphin.QuoteResponse;

public interface InsuranceCompanyApi {

    QuoteResponse getQuote(QuoteRequest quoteRequest);
}
