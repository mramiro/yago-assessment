# Getting Started

## Requirements
* java 11 or later
* a running mysql database
```
mramiro@Maximes-MacBook-Air(/Users/mramiro/sources/java/yago)
> java --version
openjdk 11.0.11 2021-04-20
OpenJDK Runtime Environment AdoptOpenJDK-11.0.11+9 (build 11.0.11+9)
OpenJDK 64-Bit Server VM AdoptOpenJDK-11.0.11+9 (build 11.0.11+9, mixed mode)
```

## Run the tests
```
mramiro@Maximes-MacBook-Air(/Users/mramiro/sources/java/yago)
> ./mvnw clean test
```

## Run the application
```
mramiro@Maximes-MacBook-Air(/Users/mramiro/sources/java/yago)
> ./mvnw spring-boot:run
```
Once the application is started, you can use the provided postman collection to call the api

## How to use
The examples hare are given with curl commands, but you can use the provided postman collection

### 1. Add your api key to be able to call the insurer
in src/main/resources/application.properties
```
insurance.seraphin.url=https://staging-gtw.seraphin.be/quotes/professional-liability
insurance.seraphin.apiToken=changeme
```

### 2. Setup the database connection
in src/main/resources/application.properties
```
spring.datasource.url=jdbc:mysql://localhost:3306/myDatabase
spring.datasource.username=root
spring.datasource.password=password

```
### 3. Create a customer 
```
curl --location --request POST 'localhost:8080/customer/' \
--header 'Content-Type: application/json' \
--data-raw '{
    "firstName": "myFirstName",
    "lastName": "myLastName",
    "email": "my.email@blabla.com",
    "phoneNumber": "+32499123456",
    "addres": "machinmachin"
}'
```

### 4. Setup a cover advice for a specific nacebel code
Those are the advices that will be taken into account before submitting a request to the insurer
```
curl --location --request POST 'http://localhost:8080/advice' \
--header 'Content-Type: application/json' \
--data-raw '{
    "nacebelId": "86210",
    "premiums": ["LEGAL_EXPENSES"],
    "deductibleFormula": "small",
    "coverageCeilingFormula": "large"
}'
```

### 5. Get a quote
You can now call the endpoint to get a quote.
The quote will also contain all the advices like LEGAL_EXPENSES in this case
```
curl --location --request POST 'http://localhost:8080/quote-with-advices' \
--header 'Content-Type: application/json' \
--data-raw '{
        "annualRevenue": 80000,
        "customerId": 12,
        "enterpriseNumber": "0649885171",
        "legalName": "example SA",
        "naturalPerson": true,
        "nacebelCodes": ["86210"]
}'

[{"id":15,"customerId":12,"coverageCeiling":800000,"deductible":10000,"quoteId":"seniorTechChallenge9588784","afterDelivery":50.0,"publicLiability":187.5,"professionalIndemnity":225.0,"entrustedObjects":62.5,"legalExpenses":75.0,"coverAdvices":[{"id":6,"nacebelId":"86210","premiums":["LEGAL_EXPENSES"],"coverageCeilingFormula":"large","deductibleFormula":"small"}]}]%                          mramiro@Maximes-MacBook-Air(/Users/mramiro/sources/java/yago)
```
And you can query this quote later by providing its id
```
curl --location --request GET 'http://localhost:8080/quote/15'

{"id":15,"customerId":12,"coverageCeiling":800000,"deductible":10000,"quoteId":"seniorTechChallenge9588784","afterDelivery":50.0,"publicLiability":187.5,"professionalIndemnity":225.0,"entrustedObjects":62.5,"legalExpenses":75.0,"coverAdvices":[{"id":6,"nacebelId":"86210","premiums":["LEGAL_EXPENSES"],"coverageCeilingFormula":"large","deductibleFormula":"small"}]}%     ```